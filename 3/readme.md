# Python - Programm-Keygen

## Ausführen

`py keygen.py`

## Dokumentation

Beim Start wird ein zufälliger 3x3 String, mit Minus**-**Zeichen dazwischen, generiert, ähnlich einem Programm mit Aktivierung.